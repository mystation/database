#!/bin/bash
# /mystation/database/init.sh

# Expects 2 parameters: username and password

if [ "$#" -ne 2 ]; then
 echo -e "\e[31mInvalid number of parameters\e[0m"
 echo -e "\e[33mUsage: ./init.sh <username> <password>\e[0m"
 exit 1
fi

MYSQL_USER=$1
MYSQL_PSWD=$2

# Start
echo -e "\e[33mSTART\e[0m"

# Create database
echo -e "\e[36m- Create MyStation database\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < database.sql

# Create tables
echo -e "\e[36m- Create table app_pancakes\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_app_pancakes.sql

echo -e "\e[36m- Create table app_resources\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_app_resources.sql

echo -e "\e[36m- Create table apps\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_apps.sql

echo -e "\e[36m- Create table oauth_access_tokens\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_oauth_access_tokens.sql

echo -e "\e[36m- Create table oauth_auth_codes\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_oauth_auth_codes.sql

echo -e "\e[36m- Create table oauth_clients\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_oauth_clients.sql

echo -e "\e[36m- Create table oauth_personal_access_clients\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_oauth_personal_access_clients.sql

echo -e "\e[36m- Create table oauth_refresh_tokens\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_oauth_refresh_tokens.sql

echo -e "\e[36m- Create table password_resets\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_password_resets.sql

echo -e "\e[36m- Create table processes\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_processes.sql

echo -e "\e[36m- Create table system_data\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_system_data.sql

echo -e "\e[36m- Create table user_apps\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_user_apps.sql

echo -e "\e[36m- Create table user_pancakes\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_user_pancakes.sql

echo -e "\e[36m- Create table users\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/table_users.sql

# Foreign keys
echo -e "\e[36m- Apply constraints for table app_pancakes\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/fk_app_pancakes.sql

echo -e "\e[36m- Apply constraints for table app_resources\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/fk_app_resources.sql

echo -e "\e[36m- Apply constraints for table user_apps\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/fk_user_apps.sql

echo -e "\e[36m- Apply constraints for table user_pancakes\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < tables/fk_user_pancakes.sql

# Procedures
echo -e "\e[36m- Compile procedure: getDatabaseInfos\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < procedures/getDatabaseInfos.sql

echo -e "\e[36m- Compile procedure: getSystemData\e[0m"
mysql -u $MYSQL_USER -p$MYSQL_PSWD < procedures/getSystemData.sql

# End
echo -e "\e[33mEND\e[0m"