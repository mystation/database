--
-- Procedure : getSystemData
-- Description : Returns 'system_data' table data in JSON format
--

USE `mystation`;

DELIMITER $$

DROP PROCEDURE IF EXISTS `getSystemData`$$
CREATE PROCEDURE `getSystemData` (IN `format` VARCHAR(4))  NO SQL
IF (format = 'JSON') THEN

    SELECT concat(
        '{ ', 
        group_concat(concat('"', system_key, '": "', value,'"') separator ', '), 
        ' }'
    ) as json_object FROM ms_system_data;

ELSE

	SELECT group_concat(concat('"', system_key, '": "', value,'"') separator ', ') as system_data FROM ms_system_data;

END IF$$

DELIMITER ;