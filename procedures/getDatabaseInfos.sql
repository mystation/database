--
-- Procedure : getDatabaseInfos
-- Description : Returns apps table infos
--

USE `mystation`;

DELIMITER $$

DROP PROCEDURE IF EXISTS `getDatabaseInfos`$$
CREATE PROCEDURE `getDatabaseInfos` ()  READS SQL DATA
BEGIN
    
    DECLARE finished INTEGER DEFAULT 0;
    DECLARE appName VARCHAR(255) DEFAULT "";
    DECLARE tableName VARCHAR(255) DEFAULT "";
    DECLARE tableSize FLOAT(10) DEFAULT 0;

    -- declare cursor for app resources tables
    DEClARE tableNameCursor
        CURSOR FOR 
            SELECT app_name, table_name
            FROM mystation.app_resources;

    -- declare NOT FOUND handler
    DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
        
    CREATE TEMPORARY TABLE temp(
        `app_name` VARCHAR(255),
        `table_name` VARCHAR(255),
        `size` FLOAT(10),
        `rows` INTEGER
    );

    OPEN tableNameCursor;
    
    getTableInfos: LOOP
    	-- fetch cursor
		FETCH tableNameCursor INTO appName, tableName;
		IF finished = 1 THEN 
			LEAVE getTableInfos;
		END IF;
        -- get table sizes
        SELECT (ist.data_length + ist.index_length) / 1024 / 1024 AS 'size' INTO tableSize
    	FROM information_schema.TABLES as ist
    	WHERE ist.table_name = tableName;
        -- get rows number
        SET @sql = CONCAT('SELECT COUNT(*) INTO @tableRows FROM ', tableName, ';');
        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
        -- insert into temp table
		INSERT INTO temp VALUES(appName, tableName, tableSize, @tableRows);
	END LOOP getTableInfos;

    CLOSE tableNameCursor;
    
    SELECT * FROM temp;
    
    DROP TEMPORARY TABLE temp;
END$$

DELIMITER ;
