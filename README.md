# MyStation database

MyStation database sources

## Initialize database

Replace `<username` and `<password>` by MySQL credentials:

```bash
./init.sh <username> <password>
```

## Dump database

Replace `<username` and `<password>` by MySQL credentials:

```bash
./dump.sh <username> <password>
```

This will create a database dump named following the format `mystation_YYYY_MM_DD_HHMMSS.sql` in `dumps` folder.