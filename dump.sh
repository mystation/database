#!/bin/bash
# /mystation/database/dump.sh

# Expects 2 parameters: username and password

if [ "$#" -ne 2 ]; then
 echo -e "\e[31mInvalid number of parameters\e[0m"
 echo -e "\e[33mUsage: ./dump.sh <username> <password>\e[0m"
 exit 1
fi

MYSQL_USER=$1
MYSQL_PSWD=$2

CURRENT_DATE=$(date '+%Y_%m_%d_%H%M%S')
DUMP_NAME="mystation_$CURRENT_DATE.sql"

# Dump database
echo -e "\e[36m- Dump MyStation database into:\e[0m dumps/$DUMP_NAME"
mysqldump -u$MYSQL_USER -p$MYSQL_PSWD mystation > dumps/$DUMP_NAME
