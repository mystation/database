--
-- Table `system_data`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `system_data` (
  `system_key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`system_key`),
  UNIQUE KEY `system_key` (`system_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `system_data` (`system_key`, `value`) VALUES
('ADMIN_PSSWD_CHANGED', NULL), -- default: NULL, then replaced by update date (YYYY-MM-DD HH:MM:SS)
('APPLICATION', 'MYSTATION'),
('APP_KEY', '0000-0000-0000-0000'),
('LOGGER_LABEL', '[>MyStation!]'),
('UDP_SERVER_PORT', '7777'),
('VERSION', '1.0.0');
