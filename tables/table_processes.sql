--
-- Table `processes`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `processes` (
  `name` varchar(254) NOT NULL COMMENT 'Process unique name',
  `cmd` text NOT NULL COMMENT 'Process command',
  `alias_cmd` varchar(254) NOT NULL COMMENT 'Alias command',
  PRIMARY KEY (`name`),
  UNIQUE KEY `unique_alias_cmd` (`alias_cmd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `processes` (`name`, `cmd`, `alias_cmd`) VALUES
('discovery-server', 'mystation prun discovery-server', 'ms_discovery_server'),
('watchdog', 'mystation prun watchdog', 'ms_watchdog');
