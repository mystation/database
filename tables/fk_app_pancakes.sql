--
-- Constraints for table `app_pancakes`
--

USE `mystation`;

ALTER TABLE `app_pancakes`
  ADD CONSTRAINT `app_pancakes_app_name_foreign` FOREIGN KEY (`app_name`) REFERENCES `apps` (`name`) ON DELETE CASCADE;
