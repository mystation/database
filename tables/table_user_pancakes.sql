--
-- Table `user_pancakes`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `user_pancakes` (
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT 'User ID',
  `pancake_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App pancake name',
  `position` int(10) UNSIGNED NOT NULL COMMENT 'App pancake position on the dashboard',
  `is_activated` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`,`pancake_name`,`position`),
  KEY `user_pancakes_pancake_name_foreign` (`pancake_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
