--
-- Constraints for table `app_resources`
--

USE `mystation`;

ALTER TABLE `app_resources`
  ADD CONSTRAINT `app_resources_app_name_foreign` FOREIGN KEY (`app_name`) REFERENCES `apps` (`name`) ON DELETE CASCADE;
