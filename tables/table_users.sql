--
-- Table `users`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Username',
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Password encrypted with bcrypt',
  `firstname` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Firstname',
  `lastname` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lastname',
  `role` enum('admin','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user' COMMENT 'User role (''admin'' or ''user'')',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `username`, `password`, `firstname`, `lastname`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$PBS.Dq3AFSnsECMyehi1neGB4ncSISQZYkEOFzfrsZYLk3gHHiBmm', '', '', 'admin', NULL, '2020-06-30 07:07:18', '2020-06-30 07:07:18');
