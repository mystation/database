--
-- Constraints for table `user_pancakes`
--

USE `mystation`;

ALTER TABLE `user_pancakes`
  ADD CONSTRAINT `user_pancakes_pancake_name_foreign` FOREIGN KEY (`pancake_name`) REFERENCES `app_pancakes` (`name`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_pancakes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
