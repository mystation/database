--
-- Table `app_resources`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `app_resources` (
  `app_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App name',
  `resource` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The app resource name',
  `table_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The app resource table name',
  `prim_key` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The app resource table primary key name',
  `key_type` enum('int','string') COLLATE utf8_unicode_ci NOT NULL COMMENT 'The app resource table primary key type',
  `incrementing_value` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Determine if the primary key is an integer to auto-increment',
  `timestamps_fields` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Determine if table has timestamps fields',
  `user_related` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Determine if table has user_id related field',
  PRIMARY KEY (`app_name`,`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
