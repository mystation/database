--
-- Table `app_pancakes`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `app_pancakes` (
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The app pancake name',
  `app_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App name',
  PRIMARY KEY (`name`),
  KEY `app_pancakes_app_name_foreign` (`app_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
