--
-- Table `apps`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `apps` (
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App name',
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App displayed name',
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App version in format X.X.X',
  `mystation_version` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'MyStation min version in format X.X.X',
  `icon` blob COMMENT 'App icon',
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App color value in hexadecimal format with the ''#'' (length: 7)',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `apps_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
