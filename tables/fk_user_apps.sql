--
-- Constraints for table `user_apps`
--

USE `mystation`;

ALTER TABLE `user_apps`
  ADD CONSTRAINT `user_apps_app_name_foreign` FOREIGN KEY (`app_name`) REFERENCES `apps` (`name`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_apps_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
