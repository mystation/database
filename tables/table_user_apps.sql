--
-- Table `user_apps`
--

USE `mystation`;

CREATE TABLE IF NOT EXISTS `user_apps` (
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT 'User ID',
  `app_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'App name',
  `position` int(10) UNSIGNED NOT NULL COMMENT 'App position',
  `is_activated` tinyint(1) NOT NULL COMMENT 'If app is activated for the user',
  `is_initialized` tinyint(1) NOT NULL COMMENT 'If app data is initialized, or for updates...',
  PRIMARY KEY (`user_id`,`app_name`),
  KEY `user_apps_app_name_foreign` (`app_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
